Visualization of relative number of reads recruited by mOTU2 hits at each analysed taxonomic rank. Hits that were not found in the annotated assembly are depicted by open symbols. 
