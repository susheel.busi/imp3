Alluvial diagram indicating contigs' membership in bins from different tools. Scaled to contig length and coloured by DASTool bin score of final DASTool bins.
