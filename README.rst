#####
IMP 3
#####

The Integrated Meta-Omics Pipeline, IMP, is a reproducible and modular pipeline for large-scale integrated analysis of coupled metagenomic and metatranscriptomic data. IMP3 incorporates read preprocessing, assembly (including optional iterative co-assembly of metagenomic and metatranscriptomic data), analysis of microbial community structure (taxonomy) and function (gene functions and abundances), binning, as well as summary and visualization.

*************
Documentation
*************

The documentation can be found `here <https://imp3.readthedocs.io/en/latest/index.html>`_.

